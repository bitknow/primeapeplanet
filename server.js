const express = require('express');
const { getRankDataFor, getRawData, getNFTConfig, getItemNumber, getRankedToken, getLegendaryTokens } = require('./service');
const path = require('path');
const fs = require('fs');
const { config } = require('process');
const app = express();
const port = 3000;
const host = '0.0.0.0';

app.engine('.ejs', require('ejs').__express);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


app.get('/', (req, res) => {
    var nftConfigs = getNFTConfig();
    res.render('index', {
        configs: Object.values(nftConfigs)
    });
});

app.get('/get_data', async (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    try {
        console.log(`fetching ${req.query.nft} data`);
        await getRawData(req.query.nft);
        res.send(JSON.stringify({status: 'success'}));
    } catch(error) {
        res.send(JSON.stringify({status: 'failed', error: error}));
    }
});


Object.values(getNFTConfig())
.map(config => config.name)
.forEach(name => {
    app.get(`/${name}`, async (req, res) => {
        try {
            var from = parseInt(req.query.from || 1);
            var to = parseInt(from) + 30;
            var config = getNFTConfig()[name];
            let items = await getRankDataFor(name, from, to);

            let rows = [];
            let i = 0;
            let step = 4;

            for (; i < items.length; i += step) {
                rows.push(items.slice(i, i+step));
            }
            if (i > items.length) {
                rows.push(items.slice(i, items.length));
            }

            res.render('list', {
                rows: rows,
                previousLink: `/${name}?from=${from-30}&to=${to-30}`,
                nextLink: `/${name}?from=${from+30}&to=${to+30}`,
                total: config.totalSupply,
                progress: await getItemNumber(name)
            });
        } catch(error) {
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({status: 'failed', error: error}));
        }
    });

    app.get(`/legendary/${name}`, async (req, res) => {
        try {
            let items = await getLegendaryTokens(name);

            let rows = [];
            let i = 0;
            let step = 4;

            for (; i < items.length; i += step) {
                rows.push(items.slice(i, i+step));
            }
            if (i > items.length) {
                rows.push(items.slice(i, items.length));
            }

            res.render('list', {
                rows: rows,
                previousLink: `/${name}/legendary`,
                nextLink: `/${name}/legendary`,
                total: items.length,
                progress: items.length
            });
        } catch(error) {
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({status: 'failed', error: error}));
        }
    });

    app.get(`/search/${name}`, async (req, res) => {
        try {
            if (req.query.id == undefined) {
                res.send(JSON.stringify({status: 'failed', error: "TOKEN ID NOT FOUND!"}));
            }

            var tokenId = parseInt(req.query.id);
            var config = getNFTConfig()[name];
            let items = await getRankedToken(name, tokenId);

            if (items.length <= 0) {
                res.send(JSON.stringify({status: 'failed', error: "TOKEN ID NOT FOUND!"}));
            }
            let rows = [[items[0]]];

            res.render('list', {
                rows: rows,
                previousLink: `/${name}`,
                nextLink: `/${name}`,
                total: config.totalSupply,
                progress: await getItemNumber(name)
            });
        } catch(error) {
            res.send(JSON.stringify({status: 'failed', error: error}));
        }
    });
});

app.listen(port, host, () => {
    console.log(`App listening at http://${host}:${port}`);
});