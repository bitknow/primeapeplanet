const utility = require('./web_utility');
const {getNFTConfig, getRawData, convertIPFSUrl, getIps} = require('./service');
const fs = require('fs');

var nftConfig = getNFTConfig();
var config = nftConfig.FancyBears;

function isKeyTx(tx) {
    var functionSignature = utility.encodeFunctionSignature(config.functionName);
    return 'to' in tx && tx.to != null && tx.to.toLowerCase() == config.contract.toLowerCase() && 'input' in tx && tx.input.length > 32 && tx.input.startsWith(functionSignature);
}

async function ethTxTrack() {
    var blockNumber = 0;
    while(true) {
        var block = await utility.getBlock('pending');
        if (blockNumber < block.number) {
            blockNumber = block.number
            console.log(`Processing block ${blockNumber}`);
        } else {
            continue
        }
        for(var i = 0; i < block.transactions.length; i++) {
            var txHash = block.transactions[i];
            var tx = await utility.getTransaction(txHash);
            if (tx != null && isKeyTx(tx)) {
                var args = config.functionArgs;
                var result = utility.decodeParameters(args, tx.input.slice(10));
                var uri = result[config.functionArgPos];
                if (config.updateUrl) {
                    console.log(`Config updated:`);
                    config.urlTemplate = convertIPFSUrl(uri);
                }
                console.log(config);
                await getRawData();
            } 
        }
    }
}

(async () => {
    console.log(data.split('\n'));
})();