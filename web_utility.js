const Web3 = require('web3');
const fs = require('fs');
const BN = require('bn.js');
const pinoms = require('pino-multi-stream');
var _ = require('lodash');

var prettyStream = pinoms.prettyStream();
const streams = [
    {stream: prettyStream},
    {stream: fs.createWriteStream('./web_utility.log', {flags: 'a'})}
]

const logger = pinoms(pinoms.multistream(streams));


// var url = 'https://rinkeby.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161';
// var chainId = 4;

var url = 'https://mainnet.infura.io/v3/0ea7732bbb43428081353b551fedd52c';
var chainId = 1;

const web3 = new Web3(new Web3.providers.HttpProvider(url));

function createAccount(num) {
    return _.map(_.range(0, num), (x) => {
        return web3.eth.accounts.create();
    });
}

async function getBlock(blockNumber) {
    return await web3.eth.getBlock(blockNumber);
}

async function getTransaction(txHash) {
    return await web3.eth.getTransaction(txHash);
}

function encodeFunctionSignature(functionName) {
    return web3.eth.abi.encodeFunctionSignature(functionName);
}

function decodeParameters(types, value) {
    return web3.eth.abi.decodeParameters(types, value);
}

function addAccount(privateKey) {
    web3.eth.accounts.wallet.add(privateKey);
}

function getWallet() {
    return web3.eth.accounts.wallet;
}

function getAccountByIndex(index) {
    return web3.eth.accounts.wallet[index];
}

function getAccountByPublicKey(publicKey) {
    return web3.eth.accounts.wallet[publicKey];
}

function toWei(number, unit) {
    return web3.utils.toWei(number, unit);
}

function fromWei(number, unit) {
    return web3.utils.fromWei(number, unit);
}

async function getBalance(address) {
    return await web3.eth.getBalance(address);
}

async function getNonce(address) {
    return await web3.eth.getTransactionCount(address);
}

async function getSign(msg, address) {
    return await web3.eth.sign(msg, address);
}

async function getPersonalSign(msg, address, password) {
    return await web3.eth.personal.sign(msg, address, password);
}

async function getEcRecover(msg, sig) {
    return await web3.eth.personal.ecRecover(msg, sig);
}

async function getRecover(msg, sig) {
    return await web3.eth.accounts.recover(msg, sig, false)
}

function utf8ToHex(msg) {
    return web3.utils.utf8ToHex(msg);
}

async function transfer(from, to, amount, nonce = undefined, gas = undefined, gasPrice = undefined) {
    var fromAddress = from;
    var toAddress = to;
    if (gas !== undefined) {
        gas = new BN(gas);
    } else {
        gas = new BN('21000');
    }
    if (gasPrice !== undefined) {
        gasPrice = new BN(gasPrice);
    } else {
        gasPrice = new BN(await getGasPrice());
    }
    var value = new BN(toWei(amount, 'ether'));
    var transaction = {
        from: fromAddress,
        to: toAddress,
        value: value.toString(10),
        gas: gas.toString(10),
        gasPrice: gasPrice.toString(10),
        chainId: chainId
    }
    if (nonce !== undefined) {
        transaction.nonce = nonce;
    }
    logger.info(transaction);
    var result = await sendTransaction(transaction);
    logger.info(result);
}

async function transferAll(from, to, nonce = undefined, gas = undefined, gasPrice = undefined) {
    var fromAddress = from;
    var toAddress = to;
    var amount = await getBalance(fromAddress);
    if (gas !== undefined) {
        gas = new BN(gas);
    } else {
        gas = new BN('21000');
    }
    if (gasPrice !== undefined) {
        gasPrice = new BN(gasPrice);
    } else {
        gasPrice = new BN(await getGasPrice());
    }
    var value = new BN(amount).sub(gas.mul(gasPrice));
    if (value < 0) {
        logger.error('not enough balance');
        return;
    }
    var transaction = {
        from: fromAddress,
        to: toAddress,
        value: value.toString(10),
        gas: gas.toString(10),
        gasPrice: gasPrice.toString(10),
        chainId: chainId
    }
    if (nonce !== undefined) {
        transaction.nonce = nonce;
    }
    logger.info(transaction);
    var result = await sendTransaction(transaction);
    logger.info(result);
}

async function getGasPrice() {
    return await web3.eth.getGasPrice();
}

async function sendTransaction(transaction) {
    return await web3.eth.sendTransaction(transaction);
}

module.exports = {
    createAccount,
    addAccount,
    getWallet,
    getAccountByIndex,
    getAccountByPublicKey,
    sendTransaction,
    toWei,
    getGasPrice,
    getBalance,
    transferAll,
    transfer,
    getNonce,
    fromWei,
    getPersonalSign,
    getSign,
    utf8ToHex,
    getEcRecover,
    getRecover,
    getBlock,
    getTransaction,
    encodeFunctionSignature,
    decodeParameters
}