'use strict';

const _ = require('lodash');
const fs = require('fs');
const { default: axios } = require("axios");
const Web3 = require('web3');
const path = require('path');
var request = require('superagent');
require('superagent-proxy')(request);

const NFTConfig = {
    cryptobullsociety: {
        name: 'cryptobullsociety',
        urlTemplate: 'https://cryptobullsociety.com/metadata/',
        totalSupply: 7777,
        filePath: path.join(__dirname, 'cryptobullsociety.json'),
        legendaryFilePath: path.join(__dirname, 'cryptobullsociety_legendary.json'),
        contract: '0x469823c7b84264d1bafbcd6010e9cdf1cac305a3',
        openseaLink: 'https://opensea.io/assets/0x469823c7b84264d1bafbcd6010e9cdf1cac305a3/',
        functionName: '',
        functionArgs: [],
        functionArgPos: 0,
        updateUrl: false,
        extension: '.json'
    },
    primeapeplanet: {
        name: 'primeapeplanet',
        urlTemplate: 'https://primeapeplanet.com/metadata/',
        totalSupply: 7979,
        filePath: path.join(__dirname, 'primeapeplanet.json'),
        legendaryFilePath: path.join(__dirname, 'primeapeplanet_legendary.json'),
        contract: '0x6632a9d63e142f17a668064d41a21193b49b41a0',
        openseaLink: 'https://opensea.io/assets/0x6632a9d63e142f17a668064d41a21193b49b41a0/',
        functionName: '',
        functionArgs: [],
        functionArgPos: 0,
        updateUrl: false,
    },
    nbayc: {
        name: 'nbayc',
        urlTemplate: 'https://ipfs.io/ipfs/QmTYSqAW6pmmeSDjeAe3DGQQ3MZBnKKzgMn4EqVb3TBWcC/',
        totalSupply: 5000,
        filePath: path.join(__dirname, 'nbayc.json'),
        legendaryFilePath: path.join(__dirname, 'nbayc_legendary.json'),
        contract: '0x17b8ef727a48c47a3dd545435fdaba9194cf3899',
        openseaLink: 'https://opensea.io/assets/0x17b8ef727a48c47a3dd545435fdaba9194cf3899/',
        functionName: '',
        functionArgs: [],
        functionArgPos: 0,
        updateUrl: true,
    },
    illuminati: {
        name: 'illuminati',
        urlTemplate: 'https://niftylabs.mypinata.cloud/ipfs/QmfZdNWuwnNNkBnVdXMJe9sQtR1bGkx3CAkKu62KwLTJuG/',
        totalSupply: 8128,
        filePath: path.join(__dirname, 'illuminati.json'),
        legendaryFilePath: path.join(__dirname, 'lluminati_legendary.json'),
        contract: '0x26badf693f2b103b021c670c852262b379bbbe8a',
        openseaLink: 'https://opensea.io/assets/0x26badf693f2b103b021c670c852262b379bbbe8a/',
        functionName: '',
        functionArgs: [],
        functionArgPos: 0,
        updateUrl: false,
    },
    SamuraiCatsbyHiroAndo: {
        name: 'SamuraiCatsbyHiroAndo',
        urlTemplate: 'https://samuraicats.mypinata.cloud/ipfs/QmcFGdmKACb2mRBiGExd9Zu3dqGWYCYzitQEvQQLrk82HL/',
        totalSupply: 4747,
        filePath: path.join(__dirname, 'SamuraiCatsbyHiroAndo.json'),
        legendaryFilePath: path.join(__dirname, 'SamuraiCatsbyHiroAndo_legendary.json'),
        contract: '0xc8d2bf842b9f0b601043fb4fd5f23d22b9483911',
        openseaLink: 'https://opensea.io/assets/0xc8d2bf842b9f0b601043fb4fd5f23d22b9483911/',
        functionName: 'setBaseURI(string)',
        functionArgs: ['string'],
        functionArgPos: 0,
        updateUrl: false,
    },
    BoredBunny: {
        name: 'BoredBunny',
        urlTemplate: 'https://ipfs.io/ipfs/QmResc5PHN2vqnZqFfkeMgNbsyiLmhxAqgPPAmNKGVu76H/',
        totalSupply: 4999,
        filePath: path.join(__dirname, 'BoredBunny.json'),
        legendaryFilePath: path.join(__dirname, 'BoredBunny_legendary.json'),
        contract: '0x9372b371196751dd2f603729ae8d8014bbeb07f6',
        openseaLink: 'https://opensea.io/assets/0x9372b371196751dd2f603729ae8d8014bbeb07f6/',
        functionName: 'setBaseURI(string)',
        functionArgs: ['string'],
        functionArgPos: 0,
        updateUrl: true,
        extension: '.json'
    },
    FancyBears: {
        name: 'FancyBears',
        urlTemplate: 'https://api.fancybearsmetaverse.com/',
        totalSupply: 7888,
        filePath: path.join(__dirname, 'FancyBears.json'),
        legendaryFilePath: path.join(__dirname, 'FancyBears_legendary.json'),
        contract: '0x87084ec881d5a15c918057f326790db177d218f2',
        openseaLink: 'https://opensea.io/assets/0x87084ec881d5a15c918057f326790db177d218f2/',
        functionName: 'setBaseURI(string)',
        functionArgs: ['string'],
        functionArgPos: 0,
        updateUrl: false,
        extension: ''
    },
    ChubbyKaijuDAO: {
        name: 'ChubbyKaijuDAO',
        urlTemplate: 'https://raw.githubusercontent.com/KaijuDAO/kaijudao/main/tokenuri/',
        totalSupply: 9999,
        filePath: path.join(__dirname, 'ChubbyKaijuDAO.json'),
        legendaryFilePath: path.join(__dirname, 'FancyBears_legendary.json'),
        contract: '0x87084ec881d5a15c918057f326790db177d218f2',
        openseaLink: 'https://opensea.io/assets/0x87084ec881d5a15c918057f326790db177d218f2/',
        functionName: 'setBaseURI(string)',
        functionArgs: ['string'],
        functionArgPos: 0,
        updateUrl: false,
        extension: ''
    },
    OxyaOrigin: {
        name: 'OxyaOrigin',
        urlTemplate: 'https://ipfs.io/ipfs/QmaiUPRjh2x1nZM7TCb4Vu5nwyHhhWjL2zvkmLd67z98eY/', 
        totalSupply: 7862,
        filePath: path.join(__dirname, 'OxyaOrigin.json'),
        legendaryFilePath: path.join(__dirname, 'OxyaOrigin_legendary.json'),
        contract: '0xe106c63e655df0e300b78336af587f300cff9e76',
        openseaLink: 'https://opensea.io/assets/0xe106c63e655df0e300b78336af587f300cff9e76/',
        functionName: '',
        functionArgs: [],
        functionArgPos: 0,
        updateUrl: false,
        extension: ''
    }
}

function getNFTConfig() {
    return NFTConfig;
}

function convertIPFSUrl(url) {
    if (url.startsWith('ipfs')) {
        return url.replace('ipfs://', 'https://ipfs.io/ipfs/');
    } else {
        return url;
    }
}

async function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function getIps(num) {
    var url = `http://tiqu.ipidea.io:81/abroad?num=${num}&type=2&lb=1&sb=0&flow=1&regions=&port=1`;
    var resp = await axios(url);
    if (resp.data.success) {
        return resp.data.data;
    } else {
        return [];
    }
}

async function getNFTSWithIP(tasks, step) {
    var userAgents = fs.readFileSync('./user_agents.txt', {encoding: 'utf-8', flag: 'r'}).split('\n');
    var ips = await getIps(500);
    var resps = [];
    var failedTasks = [];
    for (var i = 0; i < tasks.length; i += step) {
        var end = i + step;
        if (end > tasks.length) {
            end = tasks.length;
        }
        var startTime = Date.now();

        var localResps = await Promise.allSettled(
            _.range(i, end).map(n => request.get(tasks[n].url)
                .proxy(`http://${ips[n%500]['ip']}:${ips[n%500]['port']}`)
                .timeout(10000)
                .set({
                    'User-Agent': userAgents[n%1000],
                })
                .send()));
        var endTime = Date.now();
        console.log(`Time cost of ${step} requests: ${endTime - startTime}`);
        resps = resps.concat(localResps);
        localResps.forEach((resp, index) => {
            if (!('value' in resp)) {
                failedTasks.push(tasks[index]);
            }
        });
        console.log(`${i}, ${end}, ${failedTasks.length}`);
    }
    resps.forEach((resp, index) => {
        if ('value' in resp) {
            resp.value.data = JSON.parse(resp.value.text);
            if (!resp.value.data.name.includes('#')) {
                resp.value.data.name = resp.value.data.name + ` #${tasks[index].tokenId}`;
            }
        }
    });
    return {
        resps: resps.filter(resp => 'value' in resp),
        failedCount: failedTasks.length,
        failedTasks: failedTasks
    };
}

async function getNFTS(tasks, step) {
    var resps = [];
    var failedTasks = [];
    for (var i = 0; i < tasks.length; i += step) {
        var end = i + step;
        if (end > tasks.length) {
            end = tasks.length;
        }
        var startTime = Date.now();
        var localResps = await Promise.allSettled(
            _.range(i, end).map(n => axios({
                method: 'get',
                url: tasks[n].url,
                timeout: 10000
            })));
        var endTime = Date.now();
        console.log(`Time cost of ${step} requests: ${endTime - startTime}`);
        resps = resps.concat(localResps);
        localResps.forEach((resp, index) => {
            if (resp.value == undefined) {
                failedTasks.push(tasks[index]);
            }
        });
        console.log(`${i}, ${end}, ${failedTasks.length}`);
    }
    resps.forEach((resp, index) => {
        if (resp.value != undefined && !resp.value.data.name.includes('#')) {
            resp.value.data.name = resp.value.data.name + ` #${tasks[index].tokenId}`;
        } 
    });
    return {
        resps: resps.filter(resp => 'value' in resp),
        failedCount: failedTasks.length,
        failedTasks: failedTasks
    };
}

function isLegendary(item) {
    for(var i = 0; i < item.attributes.length; i++) {
        if (
            ('trait_type' in item.attributes[i] && item.attributes[i]['trait_type'].toLowerCase() == 'legendary') || 
            ('value' in item.attributes[i] && item.attributes[i]['value'].toLowerCase() == 'legendary')
            ) {
            return true
        }
    }
    return false;
}

function prefixIpfsGateways(url, n) {
    var matcher = 'https://ipfs.io/'
    var ipfsGateways = [
        'https://gateway.ipfs.io/',
        'https://ipfs.io/',
        'https://ipfs-cdn.aragon.ventures/',
        'https://ipfs.2read.net/',
        'https://hardbin.com/',
        'https://cloudflare-ipfs.com/',
        'https://ipfs.fleek.co/',
        'https://dweb.link/',
        'https://ipfs.eternum.io/',
        'https://gateway.pinata.cloud/',
        'https://infura-ipfs.io/',
        'https://ipfs.infura.io/',
        'https://storry.tv/',
    ]
    if (url.startsWith(matcher)) {
        var cid = url.slice(matcher.length);
        return ipfsGateways[n%ipfsGateways.length] + cid;
    } else {
        return url;
    }
}

async function getAllNFTs({ totalSupply, urlTemplate, filePath, legendaryFilePath, extension}) {
    var step = totalSupply;
    var tasks = _.range(1, totalSupply+1).map(n => {
        return {tokenId: n, url: urlTemplate + n + (extension ? extension : "")}
    });
    var result = {failedTasks: tasks};
    var resps = [];
    for(var i = 0; i < 1000; i++) {
        console.log(`Round #${i+1}`);

        result = await getNFTS(result.failedTasks, step);
        // result = await getNFTSWithIP(result.failedTasks, step);
        console.log(`Total failed task count: ${result.failedCount}`);
        resps = resps.concat(result.resps);

        console.log(`${urlTemplate}: ${resps.length} NFTs have acquired`)
        var items = resps.map(resp => resp.value.data);
        items.forEach(item => item.image = convertIPFSUrl(item.image));
        var legendaries = items.filter(item => {
            for (var i = 0; i < item.attributes.length; i++) {
                if ('trait_type' in item.attributes[i] && item.attributes[i]['trait_type'].toLowerCase() == 'legendary' ||
                'value' in item.attributes[i] && item.attributes[i]['value'].toLowerCase() == 'legendary') {
                    return true;
                }
            }
            return false;
        })
        var content = {
            items: items
        };

        var legendaries = items.filter(item => isLegendary(item));
        console.log(`Legendary count ${legendaries.length}`);
        fs.writeFileSync(legendaryFilePath, JSON.stringify({items: legendaries}), err => {
            if (err) {
                console.error(err);
                return
            }
        });

        console.log(`${urlTemplate}: ${resps.length} NFTs have ranked`)

        await calculateRarity(content, totalSupply, filePath);

        if (resps.length >= totalSupply) {
            break;
        }
    }
    console.log(`${urlTemplate}: ${resps.length} NFTs have acquired`)
}

function calculateRarity(data, totalSupply, filePath) {
    data.items = data.items.filter(item => 'attributes' in item);
    var traitCount = countTrait(data.items);
    var traitNotExistProbability = calcualteTraitNotExistProbability(traitCount, totalSupply);
    var traitRarity = calculateTraitRarity(traitCount, totalSupply);
    var traitCountProbability = calculateTraitCountProbability(data.items, totalSupply);
    var items = data.items.map(item => calculateItemRarity(item, traitNotExistProbability, traitRarity, traitCountProbability));
    items.sort((a, b) => {
        if (a['rarity'] > b['rarity']) {
            return -1;
        } else {
            return 1;
        }
    });
    items.forEach((item, index)=>{
        item.rank = index + 1
    })
    var content = {
        items: items
    };
    fs.writeFileSync(filePath, JSON.stringify(content), err => {
        if (err) {
            console.error(err);
            return
        }
    });
}

function calculateTraitCountProbability(items, totalSupply) {
    let traitCount = {};
    for (var item of items) {
        if (item.attributes.length in traitCount) {
            traitCount[item.attributes.length] += 1;
        } else {
            traitCount[item.attributes.length] = 1;
        }
    }
    for (var key in traitCount) {
        traitCount[key] = traitCount[key] * 1.0 / totalSupply;
    }
    return traitCount;
}

function calcualteTraitNotExistProbability(traitCount, totalSupply) {
    var traitNotExistProbability = {};
    for (let trait in traitCount) {
        var count = 0;
        for (let value in traitCount[trait]) {
            count += traitCount[trait][value];
        }
        traitNotExistProbability[trait] = (totalSupply - count) * 1.0 / totalSupply;
    }
    return traitNotExistProbability
}

function calculateItemRarity(item, traitNotExistProbability, traitRarity, traitCountProbability) {
    var traits = Object.keys(traitNotExistProbability);

    var traitMap = {};
    item.attributes.forEach(attr => {
        traitMap[attr.trait_type] = attr.value;
    });

    var rarity = 0;
    for (let trait of traits) {
        if (trait in traitMap) {
            rarity += 1 / traitRarity[trait][traitMap[trait]];
        } else {
            rarity += 1 / traitNotExistProbability[trait];
        }
    }
    item['rarity'] = rarity + 1 / traitCountProbability[item.attributes.length];
    return item;
}

function countTrait(items) {
    var traitRarity = {};
    items.forEach(item => {
        item.attributes.forEach(attr => {
            if (attr.trait_type in traitRarity) {
                if (attr.value in traitRarity[attr.trait_type]) {
                    traitRarity[attr.trait_type][attr.value] += 1;
                } else {
                    traitRarity[attr.trait_type][attr.value] = 1;
                }
            } else {
                traitRarity[attr.trait_type] = {};
                traitRarity[attr.trait_type][attr.value] = 1;
            }
        });
    });
    return traitRarity;
}

function calculateTraitRarity(traitCount, totalSupply) {
    var traitRarity = JSON.parse(JSON.stringify(traitCount));
    for (let traitType in traitCount) {
        for (let traitValue in traitCount[traitType]) {
            traitRarity[traitType][traitValue] = traitCount[traitType][traitValue] * 1.0 / totalSupply
        }
    }
    return traitRarity;
}

async function retrieveAssetsFromOpeansea({ contract, tokenIds}) {
    var urlPart1 = 'https://api.opensea.io/api/v1/assets?';
    var urlPart2 = `asset_contract_address=${contract}&order_direction=desc&offset=0&limit=30`
    for (var i = 0; i < tokenIds.length; i++) {
        urlPart1 = urlPart1 + 'token_ids=' + tokenIds[i] + '&';
    }
    var url = urlPart1 + urlPart2;
    var resp = await axios(url);
    if (resp.status == 200) {
        return resp.data;
    } else {
        return null;
    }
}

async function getAssetCurrentPrice({ contract, tokenIds }) {
    var data = await retrieveAssetsFromOpeansea({ contract, tokenIds });
    if (data != null) {
        return new Map(data.assets.map(asset => {
            if (asset.sell_orders != null && 
                asset.sell_orders.length > 0) {
                if(asset.sell_orders[0].maker_relayer_fee != '0') {
                    return [
                        asset.token_id,
                        {
                            currentPrice: Web3.utils.fromWei(asset.sell_orders[0].current_price.split('.')[0], 'ether'),
                            type: 0
                        }
                    ]
                } else if(asset.sell_orders[0].taker_relayer_fee != '0') {
                    return [
                        asset.token_id,
                        {
                            currentPrice: Web3.utils.fromWei(asset.sell_orders[0].current_price.split('.')[0], 'ether'),
                            type: 1
                        }
                    ]
                } else {
                    return [
                        asset.token_id,
                        {
                            currentPrice: Web3.utils.fromWei(asset.sell_orders[0].current_price.split('.')[0], 'ether'),
                            type: 2
                        }
                    ]
                }
            } else {
                return [
                    asset.token_id,
                    {
                        currentPrice: null,
                        type: 2
                    }
                ]
            }
        }));
    } else {
        return {};
    }
}

async function getRawData(type) {
    var config = NFTConfig[type];
    console.log('Fetching metadata started');
    await getAllNFTs(config);
    console.log('Fetching metadata finsihed');
}

function getTokenId(name) {
    var parts = name.split('#');
    if (parts.length > 1) {
        return parts[1];
    } else {
        return null;
    }
}

function getItemNumber(type) {
    var config = NFTConfig[type];
    if (fs.existsSync(config.filePath)) {
        let rawData = fs.readFileSync(config.filePath);
        let data = JSON.parse(rawData);
        return data.items.length;
    } else {
        return 0;
    }
}

async function getRankData(filePath, contract, openseaLink, from, to, tokenId=undefined) {
    if (fs.existsSync(filePath)) {
        let rawData = fs.readFileSync(filePath);
        let data = JSON.parse(rawData);
        data.items = data.items.filter(item => 'attributes' in item);
        var items = [];
        if (tokenId == undefined) {
            items = data.items.slice(from-1, to-1).map((item, index) => {
                return {
                    tokenId: getTokenId(item.name),
                    name: item.name,
                    img: item.image,
                    rank: from + index
                }
            });
        } else {
            items = data.items.filter(item => getTokenId(item.name) == tokenId).map(item => {
                return {
                    tokenId: getTokenId(item.name),
                    name: item.name,
                    img: item.image,
                    rank: item.rank
                }
            })
        }

        let tokenIds = items.filter(item => item.tokenId != null).map(item => item.tokenId);
        let itemBuyPrice = await getAssetCurrentPrice({
            contract: contract,
            tokenIds: tokenIds
        });
        items.forEach(item => {
            item.currentPrice = itemBuyPrice.get(item.tokenId).currentPrice;
            item.type = itemBuyPrice.get(item.tokenId).type;
            if (item.type == 0) {
                item.buyButtonText = `Buy Now ${item.currentPrice} eth`;
            } else if (itemBuyPrice.get(item.tokenId).type == 1) {
                item.buyButtonText = `Bid ${item.currentPrice} eth`;
            } else {
                item.buyButtonText = 'N/A';
            }
            item.openseaLink = openseaLink + item.tokenId;
        });
        return items;
    } else {
        return [];
    }
}

async function getRankDataFor(type, from, to) {
    var config = NFTConfig[type];
    return await getRankData(
        config.filePath, 
        config.contract, 
        config.openseaLink,
        from, to);
}

async function getLegendaryTokens(type) {
    var config = NFTConfig[type];
    if (fs.existsSync(config.legendaryFilePath)) {
        let rawData = fs.readFileSync(config.legendaryFilePath);
        let data = JSON.parse(rawData);
        data.items = data.items.filter(item => 'attributes' in item);
        var items = data.items.map(item => {
            return {
                tokenId: getTokenId(item.name),
                name: item.name,
                img: item.image,
                rank: 1
            }
        });

        let tokenIds = items.filter(item => item.tokenId != null).map(item => item.tokenId);
        let itemBuyPrice = await getAssetCurrentPrice({
            contract: config.contract,
            tokenIds: tokenIds
        });
        items.forEach(item => {
            item.currentPrice = itemBuyPrice.get(item.tokenId).currentPrice;
            item.type = itemBuyPrice.get(item.tokenId).type;
            if (item.type == 0) {
                item.buyButtonText = `Buy Now ${item.currentPrice} eth`;
            } else if (itemBuyPrice.get(item.tokenId).type == 1) {
                item.buyButtonText = `Bid ${item.currentPrice} eth`;
            } else {
                item.buyButtonText = 'N/A';
            }
            item.openseaLink = config.openseaLink + item.tokenId;
        });
        return items;
    } else {
        return [];
    }
}

async function getRankedToken(type, tokenId) {
    var config = NFTConfig[type];
    return await getRankData(
        config.filePath, 
        config.contract, 
        config.openseaLink,
        0, 0, tokenId);
}

module.exports = {
    getRawData,
    getRankDataFor,
    getNFTConfig,
    getItemNumber,
    getRankedToken,
    getLegendaryTokens,
    convertIPFSUrl,
    sleep,
    getIps
}